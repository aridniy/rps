# Rock-Paper-Scissors 

Rock-Paper-Scissors game web application. 
It allows the user to start the game, make moves, terminate the game and observe the statistics.
No UI interface is available. 

**To start a game:**

```
POST /api/game
```

Sample response:

```json
{
    "id": "91dddba916f963f93beedd1f640b1d8f",
    "state": "RUNNING",
    "startDate": "2019-08-17 23:37:39",
    "wins": 0,
    "draws": 0,
    "loses": 0
}
```

**To make a move:**

```
PUT /api/game/{gameId}
```

request payload: 
```json
{
	"move": "ROCK" /* could be  ROCK, PAPER or SCISSORS */
}
```

Sample response:

```json
{
    "id": "91dddba916f963f93beedd1f640b1d8f",
    "state": "RUNNING",
    "startDate": "2019-08-17 23:37:39",
    "wins": 1,
    "draws": 0,
    "loses": 0,
    "pcMove": "SCISSORS",
    "lastResult": "WIN"
}
```

**To terminate the game:**

```
DELETE /api/game/{gameId}
```

Sample response:
```json
{
    "id": "91dddba916f963f93beedd1f640b1d8f",
    "state": "STOPPED",
    "startDate": "2019-08-17 23:37:39",
    "wins": 1,
    "draws": 3,
    "loses": 0
}
```

**To get games statistics:**

```
GET /api/games
```
Sample response:
```json
[
    {
        "id": "1e301411217dc0c44f1a55222280157f",
        "state": "STOPPED",
        "startDate": "2019-08-17 23:47:35",
        "draws": 0,
        "wins": 1,
        "loses": 0
    },
    {
        "id": "f5a809e92a61f4073c102e81d21914fc",
        "state": "RUNNING",
        "startDate": "2019-08-17 23:49:49",
        "draws": 0,
        "wins": 0,
        "loses": 0
    }
]
```




