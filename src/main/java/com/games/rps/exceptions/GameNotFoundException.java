package com.games.rps.exceptions;

public class GameNotFoundException extends GameException {

    public GameNotFoundException(String message) {
        super(message);
    }
}
