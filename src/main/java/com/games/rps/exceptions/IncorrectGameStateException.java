package com.games.rps.exceptions;

public class IncorrectGameStateException extends GameException {

    public IncorrectGameStateException(String message) {
        super(message);
    }
}
