package com.games.rps.data;

import com.games.rps.model.Game;

import java.util.List;

public interface GameRepository {

    Game create(Game game);

    void update(Game game);

    Game getById(String id);

    List<Game> getAllGames();
}
