package com.games.rps.data.memory;

import com.games.rps.data.GameRepository;
import com.games.rps.model.Game;
import com.games.rps.model.State;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class GameRepositoryImpl implements GameRepository {

    private final Map<String, Game> gamesMap = new ConcurrentHashMap<>();

    @Override
    public Game create(final Game game) {
        game.setId(generateId());
        gamesMap.put(game.getId(), game);
        return game;
    }

    @Override
    public void update(Game game) {
        // do nothing
    }

    @Override
    public Game getById(String id) {
        return gamesMap.get(id);
    }

    @Override
    public List<Game> getAllGames() {
        return new ArrayList<>(gamesMap.values());
    }

    private String generateId() {
        return DigestUtils.md5DigestAsHex(UUID.randomUUID().toString().getBytes());
    }
}
