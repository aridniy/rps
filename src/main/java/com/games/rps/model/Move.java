package com.games.rps.model;

public enum Move {

    ROCK,
    PAPER,
    SCISSORS;

    private Move losesTo;

    static {
        ROCK.losesTo = PAPER;
        PAPER.losesTo = SCISSORS;
        SCISSORS.losesTo = ROCK;
    }

    public boolean losesTo(Move type) {
        return losesTo == type;
    }

    public Move getLosesTo() {
        return losesTo;
    }
}
