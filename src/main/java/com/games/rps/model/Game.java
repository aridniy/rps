package com.games.rps.model;

import java.util.Date;

public class Game {

    private String id;
    private Date startDate;
    private State state;
    private int wins;
    private int draws;
    private int loses;

    private Move pcMove;
    private Result lastResult;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int draws) {
        this.draws = draws;
    }

    public int getLoses() {
        return loses;
    }

    public void setLoses(int loses) {
        this.loses = loses;
    }

    public Move getPcMove() {
        return pcMove;
    }

    public void setPcMove(Move pcMove) {
        this.pcMove = pcMove;
    }

    public Result getLastResult() {
        return lastResult;
    }

    public void setLastResult(Result lastResult) {
        this.lastResult = lastResult;
        if (lastResult == Result.WIN) {
            wins++;
        }
        if (lastResult == Result.DRAW) {
            draws++;
        }
        if (lastResult == Result.LOSE) {
            loses++;
        }
    }
}
