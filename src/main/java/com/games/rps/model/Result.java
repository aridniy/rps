package com.games.rps.model;

public enum Result {

    WIN,
    DRAW,
    LOSE
}
