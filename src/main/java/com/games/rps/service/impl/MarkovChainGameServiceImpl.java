package com.games.rps.service.impl;

import com.games.rps.data.GameRepository;
import com.games.rps.exceptions.GameException;
import com.games.rps.exceptions.GameNotFoundException;
import com.games.rps.exceptions.IncorrectGameStateException;
import com.games.rps.model.Game;
import com.games.rps.model.Move;
import com.games.rps.model.Result;
import com.games.rps.model.State;
import com.games.rps.service.GameService;
import com.games.rps.web.advices.GameErrorAdvice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class MarkovChainGameServiceImpl implements GameService {

    private static final Random RANDOM = new Random();

    private final Logger log = LoggerFactory.getLogger(GameErrorAdvice.class);
    private final Map<String, GameData> dataMap = new ConcurrentHashMap<>();

    private final GameRepository repository;

    @Autowired
    public MarkovChainGameServiceImpl(GameRepository repository) {
        this.repository = repository;
    }

    @Override
    public Game start() {
        final Game game = new Game();
        game.setStartDate(new Date());
        game.setState(State.RUNNING);
        repository.create(game);
        dataMap.put(game.getId(), new GameData());
        log.info("Start new game {}", game.getId());
        return game;
    }

    @Override
    public Game play(String id, Move move) throws GameException {
        final Game game = getGame(id);
        if (game.getState() != State.RUNNING) {
            throw new IncorrectGameStateException("Game is not in 'running' state.");
        }
        if (move == null) {
            throw new IllegalArgumentException("Move must be provided.");
        }
        final Move pcMove = nextMove(game, move);
        game.setPcMove(pcMove);
        game.setLastResult(getResult(move, pcMove));
        log.info("Make move, game {}, user - {}, pc - {}", game.getId(), move, pcMove);
        return game;
    }

    @Override
    public Game stop(String id)  throws GameException {
        final Game game = getGame(id);
        if (game.getState() == State.STOPPED) {
            throw new IncorrectGameStateException("Game is already stopped.");
        }
        log.info("Stop game {}", game.getId());
        game.setState(State.STOPPED);
        game.setPcMove(null);
        game.setLastResult(null);
        repository.update(game);
        dataMap.remove(id);
        return game;
    }

    @Override
    public List<Game> getAllGames() {
        final List<Game> allGames = repository.getAllGames();
        allGames.sort(Comparator.comparing(Game::getStartDate));
        return allGames;
    }

    private Game getGame(final String id) throws GameNotFoundException {
        final Game game = repository.getById(id);
        if (game == null) {
            throw new GameNotFoundException("Game was not found.");
        }
        return game;
    }

    private Move nextMove(final Game game, final Move userMove) {

        final GameData gameData = dataMap.get(game.getId());

        if (gameData.lastUserMove == null) {
            // first move is random
            final Move pcMove = Move.values()[RANDOM.nextInt(Move.values().length)];
            gameData.lastUserMove = pcMove;
            return pcMove;
        }

        int nextIndex = 0;
        for (int i = 0; i < Move.values().length; i++) {
            int prevIndex = gameData.lastUserMove.ordinal();
            if (gameData.markovChain[prevIndex][i] > gameData.markovChain[prevIndex][nextIndex]) {
                nextIndex = i;
            }
        }
        final Move nextUserMove = Move.values()[nextIndex];

        // choosing move user loses to
        final Move pcMove = nextUserMove.getLosesTo();

        // update chain
        gameData.markovChain[gameData.lastUserMove.ordinal()][userMove.ordinal()]++;
        gameData.lastUserMove = userMove;

        return pcMove;
    }

    private Result getResult(final Move userMove, final Move pcMove) {
        if (userMove == pcMove) {
            return Result.DRAW;
        }
        if (pcMove.getLosesTo() == userMove) {
            return Result.WIN;
        }
        return Result.LOSE;
    }

    private static class GameData {

        Move lastUserMove;
        int[][] markovChain;

        GameData() {
            final int length = Move.values().length;
            markovChain = new int[length][length];
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    markovChain[i][j] = 0;
                }
            }
        }
    }
}
