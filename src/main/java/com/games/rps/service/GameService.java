package com.games.rps.service;

import com.games.rps.exceptions.GameException;
import com.games.rps.model.Game;
import com.games.rps.model.Move;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GameService {

    Game start();

    Game play(String id, Move move) throws GameException;

    Game stop(String id) throws GameException;

    List<Game> getAllGames() throws GameException;
}
