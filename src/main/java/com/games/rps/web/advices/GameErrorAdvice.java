package com.games.rps.web.advices;

import com.games.rps.exceptions.GameNotFoundException;
import com.games.rps.exceptions.IncorrectGameStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GameErrorAdvice {

    private final Logger log = LoggerFactory.getLogger(GameErrorAdvice.class);

    @ExceptionHandler({GameNotFoundException.class})
    public ResponseEntity<String> notFound(GameNotFoundException e) {
        return error(HttpStatus.NOT_FOUND, e);
    }

    @ExceptionHandler({IncorrectGameStateException.class})
    public ResponseEntity<String> incorrectState(IncorrectGameStateException e) {
        return error(HttpStatus.CONFLICT, e);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<String> illegalArguments(IllegalArgumentException e) {
        return error(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<String> error(Exception e) {
        log.error(e.getMessage(), e);
        return error(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    private ResponseEntity<String> error(HttpStatus status, Exception e) {
        return ResponseEntity.status(status).body(e.getMessage());
    }
}
