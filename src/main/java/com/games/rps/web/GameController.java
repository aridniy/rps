package com.games.rps.web;

import com.games.rps.exceptions.GameException;
import com.games.rps.service.GameService;
import com.games.rps.web.model.GameDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class GameController {

    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping(value = "/game")
    public ResponseEntity<GameDTO> startGame() {

        final GameDTO response = new GameDTO(gameService.start());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping(value = "/game/{id}")
    public ResponseEntity<GameDTO> play(@PathVariable("id") String id,
                                        @RequestBody GameDTO request) throws GameException {

        final GameDTO response = new GameDTO(gameService.play(id, request.getMove()));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/game/{id}")
    public ResponseEntity<GameDTO> stopGame(@PathVariable("id") String id) throws GameException {

        final GameDTO response = new GameDTO(gameService.stop(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/games")
    public ResponseEntity<List<GameDTO>> getGameStats() throws GameException {

        final List<GameDTO> response = gameService.getAllGames()
                .stream().map(GameDTO::new).collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
