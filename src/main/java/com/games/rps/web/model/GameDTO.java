package com.games.rps.web.model;

import com.games.rps.model.Game;
import com.games.rps.model.Move;
import com.games.rps.model.Result;
import com.games.rps.model.State;

import java.util.Date;

public class GameDTO {

    private Game game;

    private Move move;

    public GameDTO() {
        game = new Game();
    }

    public GameDTO(Game game) {
        this.game = game;
    }

    public String getId() {
        return game.getId();
    }

    public Date getStartDate() {
        return game.getStartDate();
    }

    public State getState() {
        return game.getState();
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public int getWins() {
        return game.getWins();
    }

    public int getDraws() {
        return game.getDraws();
    }

    public Move getPcMove() {
        return game.getPcMove();
    }

    public int getLoses() {
        return game.getLoses();
    }

    public Result getLastResult() {
        return game.getLastResult();
    }
}
