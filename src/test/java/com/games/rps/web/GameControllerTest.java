package com.games.rps.web;

import com.games.rps.exceptions.GameNotFoundException;
import com.games.rps.exceptions.IncorrectGameStateException;
import com.games.rps.model.Game;
import com.games.rps.model.Move;
import com.games.rps.model.Result;
import com.games.rps.model.State;
import com.games.rps.service.GameService;
import com.games.rps.web.model.GameDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class GameControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private GameService gameService;
    private GameController controller;

    @Before
    public void setup() {
        controller = new GameController(gameService);
    }

    @Test
    public void startGame() {

        final Game game = new Game();
        game.setId("123");
        game.setState(State.RUNNING);

        when(gameService.start()).thenReturn(game);

        final ResponseEntity<GameDTO> response = controller.startGame();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("123", response.getBody().getId());
        assertEquals(State.RUNNING, response.getBody().getState());
    }

    @Test
    public void play() throws Exception {

        final Game game = new Game();
        game.setId("123");
        game.setState(State.RUNNING);
        game.setLastResult(Result.WIN);

        when(gameService.play("123", Move.PAPER)).thenReturn(game);

        mvc.perform(put("/api/game/123")
                .contentType(MediaType.APPLICATION_JSON).content("{\"move\":\"PAPER\"}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(containsString("\"id\":\"123\"")))
                .andExpect(content().string(containsString("\"lastResult\":\"WIN\"")))
                .andExpect(content().string(containsString("\"state\":\"RUNNING\"")));
    }

    @Test
    public void play_BadRequest() throws Exception {

        when(gameService.play("123", null)).thenThrow(new IllegalArgumentException("Move is required."));

        mvc.perform(put("/api/game/123")
                .contentType(MediaType.APPLICATION_JSON).content("{}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Move is required."));
    }

    @Test
    public void stopGame() throws Exception {

        final Game game = new Game();
        game.setId("123");
        game.setState(State.STOPPED);

        when(gameService.stop("123")).thenReturn(game);

        mvc.perform(delete("/api/game/123"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(containsString("\"id\":\"123\"")))
                .andExpect(content().string(containsString("\"state\":\"STOPPED\"")));
    }

    @Test
    public void stopGame_GameNotFound() throws Exception {

        when(gameService.stop("123"))
                .thenThrow(new GameNotFoundException("Game not found."));

        mvc.perform(delete("/api/game/123"))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Game not found."));
    }

    @Test
    public void stopGame_IncorrectState() throws Exception {

        when(gameService.stop("123"))
                .thenThrow(new IncorrectGameStateException("State is not correct."));

        mvc.perform(delete("/api/game/123"))
                .andExpect(status().isConflict())
                .andExpect(content().string("State is not correct."));
    }

    @Test
    public void getGames() throws Exception {

        final Game gameOne = new Game();
        gameOne.setId("1");
        final Game gameTwo = new Game();
        gameTwo.setId("2");
        when(gameService.getAllGames())
                .thenReturn(Arrays.asList(gameOne, gameTwo));

        mvc.perform(get("/api/games"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"id\":\"1\"")))
                .andExpect(content().string(containsString("\"id\":\"2\"")));
    }
}
